CFLAGS=""

blackholed: blackholed.c
	${CC} -o $@ $^

.PHONY: clean

clean:
	\rm blackholed
