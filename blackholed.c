#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<time.h>

#define CLFDS 3

void infiniti(){
    //while(1){
    //}
    sleep(10);
}

int main(){

    chdir("/tmp");
    if(fork() == 0){
        int i;
        setsid();
        if(fork() == 0){
            setpgid(getpid());
            mode_t wwash = 0022;
            umask(wwash);
            for(i=0;i<CLFDS;i++){
               close(i);
            }
            int fd0 = open("/dev/null",S_IRUSR|S_IWUSR|S_IRGRP|S_IWGRP|S_IROTH|S_IWOTH);
            int fd1 = dup(fd0);
            int fd2 = dup(fd1);

            infiniti();
        }
        else{
            exit(0);
        }
    }
    else{
        exit(0);
    }
    return(0);
}
